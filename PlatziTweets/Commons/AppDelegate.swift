//
//  AppDelegate.swift
//  PlatziTweets
//
//  Created by Jontahan Ixcayau on 8/12/20.
//  Copyright © 2020 Jontahan Ixcayau. All rights reserved.
//

import UIKit
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        FirebaseApp.configure()
        return true
    }
}
