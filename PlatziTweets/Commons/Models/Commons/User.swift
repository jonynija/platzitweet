//
//  User.swift
//  PlatziTweets
//
//  Created by Jontahan Ixcayau on 8/16/20.
//  Copyright © 2020 Jontahan Ixcayau. All rights reserved.
//

import Foundation

struct User: Codable {
    let email: String
    let names: String
    let nickname: String
}
