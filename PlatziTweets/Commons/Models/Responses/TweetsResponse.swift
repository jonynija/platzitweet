//
//  GetTweets.swift
//  PlatziTweets
//
//  Created by Jontahan Ixcayau on 8/16/20.
//  Copyright © 2020 Jontahan Ixcayau. All rights reserved.
//

import Foundation

struct TweetsResponse: Codable {
    let twetts = Array<Tweet>()
}

