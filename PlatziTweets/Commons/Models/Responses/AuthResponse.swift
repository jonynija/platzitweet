//
//  AuthResponse.swift
//  PlatziTweets
//
//  Created by Jontahan Ixcayau on 8/16/20.
//  Copyright © 2020 Jontahan Ixcayau. All rights reserved.
//

import Foundation

struct AuthResponse: Codable {
    let user: User
    let token: String
}

