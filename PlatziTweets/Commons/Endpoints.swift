//
//  Endpoints.swift
//  PlatziTweets
//
//  Created by Jontahan Ixcayau on 8/16/20.
//  Copyright © 2020 Jontahan Ixcayau. All rights reserved.
//

import Foundation

struct Endpoints {
    static let domain: String = "https://platzi-tweets-backend.herokuapp.com/api/v1/"
    static let login: String = Endpoints.domain + "auth"
    static let register: String = Endpoints.domain + "register"
    static let getPosts: String = Endpoints.domain + "posts"
    static let post: String = Endpoints.domain + "posts"
    static let delete: String = Endpoints.domain + "posts/"
}
