//
//  TweetTableViewCell.swift
//  PlatziTweets
//
//  Created by Jontahan Ixcayau on 8/16/20.
//  Copyright © 2020 Jontahan Ixcayau. All rights reserved.
//

import UIKit
import Kingfisher

class TweetTableViewCell: UITableViewCell {
    // MARK: IBOUTLETS
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var nickNameLabel: UILabel!
    @IBOutlet weak var msgLabel: UILabel!
    @IBOutlet weak var tweetImage: UIImageView!
    @IBOutlet weak var videoButton: UIButton!
    @IBOutlet weak var dateLabel: UILabel!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setupCellWith(post: Tweet) {
        nameLabel.text = post.author.names
        nickNameLabel.text = post.author.nickname
        msgLabel.text = post.text
        if post.hasImage {
            tweetImage.kf.setImage(with: URL(string: post.imageUrl))
            tweetImage.isHidden = false
        } else {
            tweetImage.isHidden = true
        }
        if post.hasVideo {
            videoButton.isHidden = false
        } else {
            videoButton.isHidden = true
        }
        formatDate(created: String(post.createdAt.prefix(10)))
    }

    private func formatDate(created: String) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        guard let date: Date = dateFormatter.date(from: created) else {
            return
        }

        dateFormatter.dateFormat = "MM-dd-yyyy"

        dateLabel.text = dateFormatter.string(from: date).replacingOccurrences(of: "-", with: "/")
    }
}
