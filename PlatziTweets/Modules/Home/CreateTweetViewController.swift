//
//  CreateTweetViewController.swift
//  PlatziTweets
//
//  Created by Jontahan Ixcayau on 8/16/20.
//  Copyright © 2020 Jontahan Ixcayau. All rights reserved.
//

import UIKit
import Simple_Networking
import SVProgressHUD
import NotificationBannerSwift
import FirebaseStorage

class CreateTweetViewController: UIViewController {
    // MARK: VARS
    private var alertMessage: NotificationBanner? = nil
    private var urlImage: String? = nil


    // MARK: IBOUTLETS
    @IBOutlet weak var postTextView: UITextView!
    @IBOutlet weak var peviewImageView: UIImageView!
    @IBOutlet weak var cameraButton: UIButton!
    @IBOutlet weak var tweetButton: UIButton!

    // MARK: IBACTIONS
    @IBAction func createTweet() {
        validateTweet();
    }

    @IBAction func dismissAction() {
        dismiss(animated: true, completion: nil)
    }

    @IBAction func openCameraAction() {
        openCamera()
    }

    // MARK: PROPERTIES
    private var imagePicker: UIImagePickerController?


    // MARK: VIEWDIDLOAD
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
    }

    private func setupUI() {
        cameraButton.layer.cornerRadius = 15
        tweetButton.layer.cornerRadius = 15
    }

    // MARK: FUNCTIONS

    private func validateTweet() {
        guard let tweet = trim(txt: postTextView.text), !tweet.isEmpty else {
            showAlert(msg: "El texto es requerido")
            return
        }

        if peviewImageView.image == nil {
            let request: PostRequest = PostRequest(text: tweet, imageUrl: nil, videoUrl: nil, location: nil)
            createPostService(request: request)
        } else {
            uploadImage()
        }
    }

    private func uploadImage() {
        guard let imageSaved = peviewImageView.image,
            let imageSavedData: Data = imageSaved.jpegData(compressionQuality: 0.1) else {
                return
        }

        SVProgressHUD.show()

        let metaDataConfit = StorageMetadata()
        metaDataConfit.contentType = "image/jpg"

        let storage = Storage.storage()
        let name = Int.random(in: 100...1000)


        let folderRef = storage.reference(withPath: "images/\(name).jpg")

        DispatchQueue.global(qos: .background).async {
            folderRef.putData(imageSavedData, metadata: metaDataConfit) { (metadata: StorageMetadata?, error: Error?) in
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()

                    if let error = error {
                        self.showAlert(msg: error.localizedDescription)
                    } else {
                        folderRef.downloadURL { (url: URL?, error: Error?) in
                            if url?.absoluteString != nil {
                                let tweet: String! = self.trim(txt: self.postTextView.text)

                                let request: PostRequest = PostRequest(text: tweet!, imageUrl: url?.absoluteString, videoUrl: nil, location: nil)
                                self.createPostService(request: request)
                            }
                        }
                    }
                }
            }
        }
    }

    private func createPostService(request: PostRequest) {
        SVProgressHUD.show()

        SN.post(endpoint: Endpoints.post, model: request) { (response: SNResultWithEntity<Tweet, ErrorResponse>) in
            SVProgressHUD.dismiss()

            switch response {
            case .success(_):
                self.dismiss(animated: true, completion: nil)
                break
            case .error(let error):
                self.showAlert(msg: "Ocurrio un error de conexion\n\(error.localizedDescription)", style: .danger)
                print(error)
                break
            case .errorResult(let res):
                self.showAlert(msg: res.error, style: .warning)
                break
            }
        }
    }

    private func openCamera() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.savedPhotosAlbum) {
            imagePicker = UIImagePickerController()
            imagePicker?.sourceType = .savedPhotosAlbum
//            imagePicker?.cameraFlashMode = .off
//            imagePicker?.cameraCaptureMode = .photo
//            imagePicker?.allowsEditing = true
            imagePicker?.delegate = self

            guard let imagePicker = imagePicker else {
                return
            }
            present(imagePicker, animated: true, completion: nil)
        } else {
            showAlert(msg: "Camara no disponible para el dispositivo")
        }
    }

    //MARK: COMMONS
    private func showAlert(msg: String, style: BannerStyle = .warning) {
        if alertMessage != nil && !alertMessage!.isHidden {
            alertMessage?.dismiss()
        }
        alertMessage = NotificationBanner(title: "Error", subtitle: msg, style: style)
        alertMessage?.show()
    }

    private func trim(txt: String?) -> String! {
        return txt!.trimmingCharacters(in: .whitespacesAndNewlines)
    }
}

// MARK : UIImagePickerControllerDelegate
extension CreateTweetViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {

        imagePicker?.dismiss(animated: true, completion: nil)

        if info.keys.contains(.originalImage) {
            peviewImageView.isHidden = false
            peviewImageView.image = info[.originalImage] as? UIImage

        }
    }
}
