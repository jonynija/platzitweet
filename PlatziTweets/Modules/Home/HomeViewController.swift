//
//  HomeViewController.swift
//  PlatziTweets
//
//  Created by Jontahan Ixcayau on 8/16/20.
//  Copyright © 2020 Jontahan Ixcayau. All rights reserved.
//

import UIKit
import NotificationBannerSwift
import Simple_Networking
import SVProgressHUD

class HomeViewController: UIViewController {

    // MARK: VARS
    private let cellId = "TweetTableViewCell"
    private var alertMessage: NotificationBanner? = nil;
    private var dataSource: Array = Array<Tweet>()

    // MARK: OUTLETS
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var createPostButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
    }

    override func viewDidAppear(_ animated: Bool) {
        requestPostsService()
    }


    private func setupUI() {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UINib(nibName: cellId, bundle: nil), forCellReuseIdentifier: cellId)

        createPostButton.layer.cornerRadius = 20
    }

    private func requestPostsService() {
        SVProgressHUD.show()

        SN.get(endpoint: Endpoints.getPosts) { (response: SNResultWithEntity<[Tweet], ErrorResponse>) in
            SVProgressHUD.dismiss()

            switch response {
            case . success(let res):
                self.dataSource = res
                self.tableView.reloadData()
                break
            case . error(let error):
                self.showAlert(msg: "Ocurrio un error de conexion\n\(error.localizedDescription)", style: .danger)
                print(error)
                break
            case .errorResult(let res):
                self.showAlert(msg: res.error, style: .warning)
                break
            }
        }
    }

    // MARK: COMMONS


    private func showAlert(msg: String, style: BannerStyle = .warning) {
        if alertMessage != nil {
            alertMessage?.dismiss()
        }
        alertMessage = NotificationBanner(title: "Error", subtitle: msg, style: style)
        alertMessage?.show()
    }

    private func trim(txt: String?) -> String! {
        return txt!.trimmingCharacters(in: .whitespacesAndNewlines)
    }

    private func deletePostAt(indexPath: IndexPath) {
        SVProgressHUD.show()
        let postId: String = dataSource[indexPath.row].id
        let endPoint: String = Endpoints.delete + postId

        SN.delete(endpoint: endPoint) { (response: SNResultWithEntity<PostDeleteResponse, ErrorResponse>) in
            SVProgressHUD.dismiss()

            switch response {
            case .success(_):
                self.dataSource.remove(at: indexPath.row)
                self.tableView.deleteRows(at: [indexPath], with: .left)
                break
            case .error(let error):
                self.showAlert(msg: "Ocurrio un error de conexion", style: .danger)
                print(error)
                break
            case .errorResult(let res):
                self.showAlert(msg: res.error, style: .warning)
                break
            }
        }
    }
}

// MARK: UITABLEVIEW
extension HomeViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let deleteAction: UITableViewRowAction = UITableViewRowAction(style: .destructive, title: "Borrar") { (_, _) in
            self.deletePostAt(indexPath: indexPath)
        }
        return [deleteAction]
    }

    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return dataSource[indexPath.row].author.email == "jonathan@ixcayau.com"
    }
}

// MARK: UITABLEVIEW
extension HomeViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count;
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath)

        if let cell = cell as? TweetTableViewCell {
            cell.setupCellWith(post: dataSource[indexPath.row])
        }
        return cell;
    }
}
