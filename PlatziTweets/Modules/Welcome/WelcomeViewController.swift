//
//  WelcomeViewController.swift
//  PlatziTweets
//
//  Created by Jontahan Ixcayau on 8/12/20.
//  Copyright © 2020 Jontahan Ixcayau. All rights reserved.
//

import UIKit

class WelcomeViewController: UIViewController {

    @IBOutlet weak var loginButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
    }

    private func setupUI() {
        loginButton.layer.cornerRadius = 15
    }
}
