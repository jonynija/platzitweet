//
//  RegisterViewController.swift
//  PlatziTweets
//
//  Created by Jontahan Ixcayau on 8/12/20.
//  Copyright © 2020 Jontahan Ixcayau. All rights reserved.
//

import UIKit
import NotificationBannerSwift
import Simple_Networking
import SVProgressHUD

class RegisterViewController: UIViewController {

    // MARK: VARS
    private var alertMessage: NotificationBanner? = nil;

    // MARK: OUTLETS
    @IBOutlet weak var registerButton: UIButton!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!

    // MARK: ACTIONS
    @IBAction func loginButtonAction() {
        validateRegister()
    }


    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
    }

    private func setupUI() {
        registerButton.layer.cornerRadius = 15
    }

    private func validateRegister() {
        // Validations
        guard let name = trim(txt: nameTextField.text), !name.isEmpty else {
            showAlert(msg: "Debes especificar un nombre")
            return
        }

        guard let email = trim(txt: emailTextField.text), !email.isEmpty else {
            showAlert(msg: "Debes especificar un correo")
            return
        }

        if !isValidEmail(email: email) {
            showAlert(msg: "Debes especificar un correo valido")
            return
        }

        guard let password = trim(txt: passwordTextField.text), !password.isEmpty else {
            showAlert(msg: "Debes especificar una contraseña")
            return
        }

        // Clear view
        if alertMessage != nil {
            alertMessage?.dismiss()
        }
        view.endEditing(true)


        let request: RegistereRequest = RegistereRequest(email: email, password: password, names: name)
        requestRegisterService(request: request)
    }

    private func requestRegisterService(request: RegistereRequest) {
        SVProgressHUD.show()

        SN.post(endpoint: Endpoints.register, model: request) { (response: SNResultWithEntity<AuthResponse, ErrorResponse>) in
            SVProgressHUD.dismiss()

            switch response {
            case . success(let res):
                self.showAlert(msg: "Bienvenido \(res.user.names)", style: .success)
                SimpleNetworking.setAuthenticationHeader(prefix: "", token: res.token)
                self.performSegue(withIdentifier: "showHome", sender: nil)
                break
            case . error(let error):
                self.showAlert(msg: "Ocurrio un error de conexion", style: .danger)
                print(error)
                break
            case .errorResult(let res):
                self.showAlert(msg: res.error, style: .warning)
                break
            }
        }
    }


    // MARK: COMMONS

    private func isValidEmail(email: String) -> Bool {
        let emailRegEx = "(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}" + "~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\" +
            "x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-" +
            "z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5" +
            "]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-" +
            "9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21" +
            "-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"


        let emailTest = NSPredicate(format: "SELF MATCHES[c] %@", emailRegEx)
        return emailTest.evaluate(with: email)
    }

    private func showAlert(msg: String, style: BannerStyle = .warning) {
        if alertMessage != nil {
            alertMessage?.dismiss()
        }
        alertMessage = NotificationBanner(title: "Error", subtitle: msg, style: style)
        alertMessage?.show()
    }

    private func trim(txt: String?) -> String! {
        return txt!.trimmingCharacters(in: .whitespacesAndNewlines)
    }
}
